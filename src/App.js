import React, {useState } from 'react';
import './App.css';
import ShowResultComponent from './components/ShowResultComponent';
import ButtonsComponent from "./components/ButtonsComponent";

const app  = (props) =>{
 
    const [result , setResult] = useState({val :""});

   const onClick = name => {

        if(name === "="){
            calculate()
        }

        else if(name === "C"){
            reset()
        }
        else if(name === "CE"){
            backspace()
        }

        else {
            setResult({
                val: result.val + name
            })
        }
    };


    const calculate = () => {
        let checkResult = result.val;
        try {
            let res = eval(checkResult) || ""
            setResult({
                val: res + ""
            })
        } catch (e) {
            setResult({
                val: "error"
            })

        }
    };

    const reset = () => {
        setResult({
            val: ""
        })
    };

   const backspace = () => {
        setResult({
            val: result.val.slice(0, -1)
        })
    };

    
        return (
            <div>
                <div className="calculator">
                    <h1>My Calculator</h1>
                    <ShowResultComponent result={result.val}/>
                    <ButtonsComponent onClick={onClick}/>
                </div>
            </div>
        );
    
}

export default app;
